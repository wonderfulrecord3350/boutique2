/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.rs;

import com.univlome.entites.Produit;
import com.univlome.service.ProduitService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Wonderful
 */
@Path("/produitService")
public class ProduitServiceResource {
    ProduitService ps =new ProduitService();
    
    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addProduit(Produit p){
        ps.ajouter(p);
          
    }
    
    @POST
    @Path("/find")
    public void updateProduit(Produit p){
        ps.modifier(p);
        
    }
    @GET
    @Path("/find")
    @Produces("MediaType.APPLICATION_JSON")
    public Produit findProduit(@PathParam("id") int id){
        return ps.trouver(id);
        
    }
    
    @DELETE
    @Path("/deleteAvecId")
    public void deleteProduit(@PathParam("id") int id){
        ps.supprimer(id);
        
    }
    
    @DELETE
    @Path("/delete")
    public void deleteProduit(Produit p){
        ps.supprimer(p);
        
    }
    
    @GET
    @Path("/listerSimple")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Produit> getListe(){
    
        return ps.lister();
    }
    
    @GET
    @Path("/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Produit> getListe(@PathParam("debut") int debut , @PathParam("nombre") int nombre){
        return ps.lister(debut, nombre);
    }
    
    
}
