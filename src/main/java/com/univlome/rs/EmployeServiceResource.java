/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.rs;

import com.univlome.entites.Employe;
import com.univlome.service.EmployeService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Wonderful
 */
@Path("/employeservice")
public class EmployeServiceResource {
    EmployeService es =new EmployeService();
    
    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void  addCategorie(Employe e){
        es.ajouter(e);
          
    }
    
    @POST
    @Path("/update")
    public void  updateCategorie(Employe e){
        es.modifier(e);
        
    }
    @GET
    @Path("/find")
    @Produces("MediaType.APPLICATION_JSON")
    public Employe findEmploye(@PathParam("id") int id){
        return es.trouver(id);
        
    }
    
    @DELETE
    @Path("/deleteAvecId")
    public void  deleteEmploye(@PathParam("id") int id){
        es.supprimer(id);
        
    }
    
    @DELETE
    @Path("/delete")
    public void  deleteEmploye(Employe e){
        es.supprimer(e);
        
    }
    
    @GET
    @Path("/listerSimple")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Employe> getListe(){
        return es.lister();
    }
    
    @GET
    @Path("/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Employe> getListe(@PathParam("debut") int debut,@PathParam("nombre")int nombre){
        return es.lister(debut, nombre);
    }
    
    
}
