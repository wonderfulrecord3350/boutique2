/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.rs;

import com.univlome.entites.Categorie;
import com.univlome.service.CategorieService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Wonderful
 */
@Path("/categorieService")
public class CategorieServiceResource {
    CategorieService cs =new CategorieService();
    
    @POST
    @Path("/categorieService/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public  void addCategorie(Categorie e){
        cs.ajouter(e);
          
    }
    
    @POST
    @Path("/update")
    public  void updateCategorie(Categorie e){
        cs.modifier(e);
        
    }
    @GET
    @Path("/find")
    @Produces("MediaType.APPLICATION_JSON")
    public  Categorie findCategorie(int id){
        return cs.trouver(id);
        
    }
    
    @DELETE
    @Path("/deleteAvecId")
    public void  deleteCategorie(@PathParam( "d") int id ){
        cs.supprimer(id);
        
    }
    
    @DELETE
    @Path("/delete")
    public  void deleteCategorie(Categorie e){
        cs.supprimer(e);
        
    }
    
    @GET
    @Path("/listerSimple")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Categorie> getListe(){
        return cs.lister();
    }  
    
    @GET
    @Path("/lister")
    @Produces("MediaType.APPLICATION_JSON")
    public List<Categorie> getListe(@PathParam("debut ") int debut , @PathParam("nombre") int nombre){
        return cs.lister(debut, nombre);
    }
    
}
