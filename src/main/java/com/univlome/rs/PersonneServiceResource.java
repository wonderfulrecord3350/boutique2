/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.rs;

import com.univlome.entites.Personne;
import com.univlome.service.PersonneService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Wonderful
 */
@Path("/personneService")
public class PersonneServiceResource {
    PersonneService cs =new PersonneService();
    
    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void  addCategorie(Personne e){
        cs.ajouter(e);
          
    }
    
    @POST
    @Path("/update") 
    public void  updatePersonne(Personne e){
        cs.modifier(e);
        
    }
    @GET
    @Produces("MediaType.APPLICATION_JSON")
    @Path("/find")
    public Personne findPersonne(int id){
        return cs.trouver(id);
        
    }
    
    @DELETE
    @Path("/deleteAvecId")
    public void deletePersonne(@PathParam("id") int id){
        cs.supprimer(id);
        
    }
    
    @DELETE
    @Path("/delete")
    public void  deletePersonne(Personne e){
        cs.supprimer(e);
        
    }
    
    @GET
    @Produces("MediaType.APPLICATION_JSON")
    @Path("/listerSimple")
    public List<Personne> getListe(){
        return cs.lister();
    }
    
    @GET
    @Produces("MediaType.APPLICATION_JSON")
    @Path("/lister")
    public List<Personne> getListe(@PathParam("debut") int debut, @PathParam("nombre") int nombre){
        return cs.lister(debut, nombre);
    }
    
    
}
