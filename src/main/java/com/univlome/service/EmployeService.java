/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.service;

import com.univlome.entites.Employe;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author Wonderful
 */
@Path("/boutique")
public class EmployeService {
    private List<Employe> liste;
    
    public void ajouter(Employe e){
        if(liste.contains(e))
            System.out.println("l'élement existe déja ");
        else
            liste.add(e); 
        
    }
    
    public void modifier(Employe e){
        if(liste.contains(e)){
            for(Employe c :liste){
                if(c.equals(e))
                    c=e;
            }
        }
        else 
            System.out.println("Id introuvable");
        
    }
    
    public Employe trouver(int id){
         Employe etemp =new Employe();
        if(liste==null)
            return null;
        else{
            for(Employe c : liste){
                if(c.getId()==id){
                   System.out.println("Categorie trouvée");
                   etemp=c;   
                } 
                else 
                    System.out.println("Id introuvable dans la liste");
            }
            return etemp;
        }
        
        
    }
    
    public void supprimer(int id){
        if(trouver(id)!=null)
            liste.remove(trouver(id));
        
    }
    
    public void supprimer(Employe e){
        if(liste.contains(e))
            liste.remove(e);
        else
            System.out.println("cette categorie n'exite pas dans la liste");
        
    }
    
    public List<Employe> lister(){
         return new ArrayList<>((Collection<? extends Employe>) liste.iterator());
        
        
    }
    
    public List<Employe> lister(int debut, int nombre){
          int index =debut;
        List<Employe> maliste = null;
        if(liste ==null){
            System.out.println("La vide est vide ");
            return null;
        }
        else{
            if(debut >liste.size() || nombre >liste.size())
                System.out.println("La taile de la liste est debordée");
            else{
                if(index==debut){
                      do{
                    maliste.add(liste.get(index));
                    index++;
                    nombre--;    
                }while(nombre==0);
                    
                }
              
            }
        }
         
      return new ArrayList<>((Collection<? extends Employe>) maliste.iterator());
        
    }
      
        
    }
    

