/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.univlome.service;

import com.univlome.entites.Achat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.Path;

/**
 *
 * @author Wonderful
 */
@Path("/boutique")
public class AchatService {
    private List<Achat> liste;
    
    public void ajouter(Achat a){
        if(liste.contains(a))
            System.out.println("l'élement existe déja ");
        else
            liste.add(a); 
        
    }
    
    public void modifier(Achat a){
        if(liste.contains(a)){
            for(Achat c :liste){
                if(c.equals(a))
                    c=a;
            }
        }
        else 
            System.out.println("Id introuvable");
        
    }
    
    public Achat trouver(int id){
         Achat atemp =new Achat();
        if(liste==null)
            return null;
        else{
            for(Achat c : liste){
                if(c.getId()==id){
                   System.out.println("Categorie trouvée");
                   atemp=c;   
                } 
                else 
                    System.out.println("Id introuvable dans la liste");
            }
            return atemp;
        }
        
        
    }
    
    public void supprimer(int id){
        if(trouver(id)!=null)
            liste.remove(trouver(id));
        
    }
    
    public void supprimer(Achat a){
        if(liste.contains(a))
            liste.remove(a);
        else
            System.out.println("cette categorie n'exite pas dans la liste");
        
    }
    
    public List<Achat> lister(){
         return new ArrayList<>((Collection<? extends Achat>) liste.iterator());
      
        
    }
    
    public List<Achat> lister(int debut, int nombre){
          int index =debut;
        List<Achat> maliste = null;
        if(liste ==null){
            System.out.println("La vide est vide ");
            return null;
        }
        else{
            if(debut >liste.size() || nombre >liste.size())
                System.out.println("La taile de la liste est debordée");
            else{
                if(index==debut){
                      do{
                    maliste.add(liste.get(index));
                    index++;
                    nombre--;    
                }while(nombre==0);
                    
                }
              
            }
        }
         
      return new ArrayList<>((Collection<? extends Achat>) maliste.iterator());
        
    }
      
        
    }
    

